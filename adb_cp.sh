DEVICE=$1

if [ -z "$DEVICE" ]
then
  adb shell screencap -p /sdcard/screencap.png
else
  adb -s $DEVICE shell screencap -p /sdcard/screencap.png
fi

# Get the screenshot and redirect command output to nowhere so it's quiet.
if [ -z "$DEVICE" ]
then
  adb pull /sdcard/screencap.png /tmp/adb_cp.png > /dev/null
else
  adb -s $DEVICE pull /sdcard/screencap.png /tmp/adb_cp.png > /dev/null
fi

case "$(uname)" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    MSYS_NT*)   machine=Git;;
    *)          machine="UNKNOWN:${unameOut}"
esac

if [ "$machine" == "Mac" ]; then
  osascript -e 'set the clipboard to (read (POSIX file "/tmp/adb_cp.png") as «class PNGf»)'
elif [ "$machine" == "Linux"]; then
  xclip -selection clipboard -t image/png -i /tmp/adb_cp.png
else
  echo "$machine is not supported yet."
fi

echo Screenshot copied to clipboard!
